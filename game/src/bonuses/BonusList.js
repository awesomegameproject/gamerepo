/**
 * @author Marek Stereńczak
 */

awesomegame.namespace("bonuses");

( function() {

    function BonusList() {
        this._definedBonuses = [];
        this._bonuses = [];
    }


    awesomegame.bonuses.BonusList = BonusList;

    BonusList.prototype.addBonus = function(index) {

        switch (index) {
            case 0:
                return this._bonuses.push(new awesomegame.bonuses.SpeedUp(player)) - 1;
                break;
        }

    };

    BonusList.prototype.get = function(index) {
        return this._bonuses[index];
    };

    BonusList.prototype.getActiveBonuses = function() {
        this._activeBonuses = this._bonuses.filter(function(bonus) {
            return bonus.isActive;
        });

        return this._activeBonuses;
    };

    BonusList.prototype.getIndexOf = function (obj) {
        return this._bonuses.indexOf(obj);
    }

}());
