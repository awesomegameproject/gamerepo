/**
 * @author Marek Stereńczak
 */

awesomegame.namespace("bonuses");

( function () {
	
	function SpeedUp(affectedUser) {
		this._affectedUser = affectedUser;
		this._durationTime = awesomegame.constants.bonusDuration.speedUp;
		this._icon = new createjs.Bitmap("img/bonus/speedUp.jpg");
	}
	
	SpeedUp.prototype = new awesomegame.bonuses.Bonus(); // inheritance
	
	awesomegame.bonuses.SpeedUp = SpeedUp;
		
	SpeedUp.prototype.execute = function () {
		var carController = this._affectedUser.getCar().getCarController(), speedup = 2;
		
		carController.changeSpeed("+", speedup);
		
		setTimeout( function () {
			carController.changeSpeed("-", speedup);
		}, this._durationTime);
		
	};
	
	
	
} ());
