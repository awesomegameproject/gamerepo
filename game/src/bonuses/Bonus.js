/**
 * @author Marek Stereńczak
 */

awesomegame.namespace("bonuses");

( function() {

		function Bonus() {
			this._durationTime = 0;
			this._affectedUser = {};
			this._icon = {};
		}


		awesomegame.bonuses.Bonus = Bonus;

		Bonus.prototype.deactivate = function() {
			stage.removeChild(this._icon);
			this.isActive = false;

		};

		Bonus.prototype.activate = function(x, y) {

			this._icon.x = x;
			this._icon.y = y;

			stage.addChild(this._icon);

			this.isActive = true;
		};

		Bonus.prototype.execute = function() {
			// do nothing
		};

        Bonus.prototype.executeForEnemy = function() {
			// do nothing
		};


		Bonus.prototype.getComponent = function() {
			return this._icon;
		};

	}() );

