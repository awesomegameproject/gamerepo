awesomegame.namespace("view"); 

( function() {

		var _bulletComponent;

		function Bullet() {
			_initializeBullet();
		}

		function _initializeBullet() {
			_bulletComponent = new createjs.Bitmap("img/pocisk.png");
			_bulletComponent.scaleX = awesomegame.constants.bulletScaleX;
			_bulletComponent.scaleY = awesomegame.constants.bulletScaleY;
			
			_bulletComponent.regX = awesomegame.constants.bulletRegX;
			_bulletComponent.regY = awesomegame.constants.bulletRegY;
		}

		function getBulletComponent() {
			return _bulletComponent;
		}


		awesomegame.view.Bullet = Bullet;

		Bullet.prototype.getBulletComponent = getBulletComponent;

	}()); 