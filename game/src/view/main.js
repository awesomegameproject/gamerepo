var socket = io.connect(),
    player = {},
    stage,
    opponents = [],
    arrayBullet = [],
    bonusList = [],
    click = false;
collisionDetector = new awesomegame.controller.CollisionDetector(),
    boardAnimator = new awesomegame.controller.BoardAnimator();

//Disable arrow key scrolling in users browser
window.addEventListener("keydown", function(e) {
    if ([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
        e.preventDefault();
    }
}, false);

//stay alive this :)
document.getElementById("chat").addEventListener("keydown", function(e) {
    if (e.keyCode == 32) {
        document.getElementById("chatMessage").value += " ";
    } else if (e.keyCode == 13) {
        document.getElementById("sendChatMessage").click();
    }
}, false);

function addPlayer(plyr) {
    player = plyr;
}

function startSync() {
    setInterval( function () {
        socket.emit('syncPositionsRequest', '{ "id": "' + nickName + '", "room": "' + roomName + '", "x": "' +
         player.getCarComponent().x + '", "y": "' + player.getCarComponent().y + '", "rotation": "' + player.getCarComponent().rotation + '" }');
    }, awesomegame.constants.syncInterval);
}

function prepareClientServerConnection() {
    var allOpponentDeath = function(element, index, array) {
        return !element.live;
    };

    stage = new createjs.Stage("AwesomeGame");

    createjs.Sound.registerSound({
        src : "sounds/fire.ogg",
        id : "fireSound"
    });

    socket.on('helloFromServer', function(data) {
        nickName = prompt("Podaj nicka: ");
        roomName = prompt("Podaj room: ");

        document.getElementById("nick").innerHTML += "<b>" + nickName + "</b>";
        document.getElementById("room").innerHTML += "<b>" + roomName + "</b>";
        document.getElementById("round").innerHTML = "Liczba rund: <b> 1/10 </b>";

        player = new awesomegame.model.Player(nickName, roomName);

        socket.emit('joinNewPlayer', '{"id": "' + nickName + '", "room": "' + roomName + '"}');

        var p = document.createElement("p");
        p.id = "nick" + nickName;
        p.innerHTML = nickName + " - " + 0;
        p.style.color = "red";
        p.style.fontWeight = "bold";
        document.getElementById("list").appendChild(p);

        addPlayer(player);
        bonusList = new awesomegame.bonuses.BonusList();
    });

    socket.on('sendNewPlayerToAllPlayersInRoom', function(id) {

        opponent = new awesomegame.model.Opponent(id);

        opponents.push(opponent);

        var p = document.createElement("p");
        p.id = "nick" + id;
        p.innerHTML = id + " - " + 0;
        document.getElementById("list").appendChild(p);

    });

    socket.on('sendCurrentPlayersInRoomToNewPlayer', function(id) {
        opponent = new awesomegame.model.Opponent(id);

        opponents.push(opponent);

        var p = document.createElement("p");
        p.id = "nick" + id;
        p.innerHTML = id + " - " + 0;
        document.getElementById("list").appendChild(p);

    });

    socket.on('startGameFromServer', function(data) {
        stage.removeAllChildren();
        arrayBullet = [];
        for (var i in data) {
            var parser = JSON.parse(data[i]);

            if (player._nickName == parser.id) {
                player.backToLive();
                player.setPositions(parseInt(parser.x, 10), parseInt(parser.y, 10));
            } else {
                opponents.forEach(function(opponent) {
                    if (opponent._nickName === parser.id) {
                        opponent.backToLive();
                        opponent.setPositions(parseInt(parser.x, 10), parseInt(parser.y, 10));
                    }
                });
            }
        }

        document.getElementById("AwesomeGame").style.display = "block";
        startSync();
        click = true;
    });

    socket.on('sync', function(data) {
        var parser = JSON.parse(data), moveOpponent = function(opponent) {
            if (opponent._nickName === parser.id) {
                opponent.getCarController().moveTo(parseInt(parser.x), parseInt(parser.y), parseInt(parser.rotation));
            }
        };
        console.log("synchronizuje");
        opponents.forEach(moveOpponent);

    });

    socket.on('sendNewPositionsToAllPlayersInRoom', function(data) {
        var parser = JSON.parse(data), moveOpponent = function(opponent) {
            if (opponent._nickName === parser.id) {
                opponent.setLastMove(parser.lastMove);
                opponent.setSpeed(parser.speed);
            }
        };

        opponents.forEach(moveOpponent);

    });

    socket.on('sendNewBulletToAllPlayersInRoom', function(data) {
        var parser = JSON.parse(data);

        if (player._nickName !== parser.id) {
            var bullet = new awesomegame.view.Bullet();

            createjs.Sound.play("fireSound");

            bullet = bullet.getBulletComponent();
            bullet.x = parseInt(parser.x);
            bullet.y = parseInt(parser.y);
            bullet.rotation = parseInt(parser.rotation);
            stage.addChild(bullet);

            opponents.forEach(function(opponent) {
                if (opponent._nickName === parser.id) {
                    arrayBullet[arrayBullet.length] = [bullet, opponent.getCarComponent()];
                }
            });

        }

    });

    socket.on("sendNewHitToAllPlayersInRoom", function(data) {
        var parser = JSON.parse(data);

        opponents.forEach(function(opponent) {
            if (opponent._nickName === parser.id) {
                opponent.damage();
            }
        });

        stage.removeChild(arrayBullet[parseInt(parser.bulletIndex)][0]);
        arrayBullet.splice(parser.bulletKillerIndex, 1);

        if (opponents.every(allOpponentDeath)) {
            socket.emit("playerWin", '{"id":"' + player.getNick() + '", "room":"' + player.getRoom() + '"}');

            document.getElementById('startGameButton').value = "Wygrał gracz: " + player.getNick() + " .Zacznij gre od nowa!";
            document.getElementById('startGameButton').style.display = "block";
        }

    });

    socket.on("receiveBonus", function(index, x, y) {

        boardAnimator.activateBonus(index, x, y);

    });

    socket.on('playerPickedBonusFromServer', function (data) {

        var parser = JSON.parse(data),
            bonus = bonusList.get(parseInt(parser.bonus));

        if (player.getNick() === parser.id) {

            bonus.execute();
        } else {

            bonus.executeForEnemy();
        }

        bonus.deactivate();


    });

    socket.on("theEnd", function(data) {

        player.kill();

        document.getElementById('gameOver').innerHTML += data;
        document.getElementById('AwesomeGame').style.display = "none";
        document.getElementById('sendChatMessage').disabled = "true";
        document.getElementById('gameOver').style.display = "block";
        document.getElementById('lol').style.display = "none";
        document.getElementById('round').innerHTML = "Koniec gry :)";
    });

    document.getElementById("sendChatMessage").onclick = function() {
        var message = document.getElementById("chatMessage"),
            nick = player.getNick(), room = player.getRoom(),
            chat = document.getElementById("chatMessages");

        chat.innerHTML += "<b>" + nick + ":</b> " + message.value + "<br/>";
        chat.scrollTop = chat.scrollHeight;
        socket.emit("chatSendMessageToServer", message.value, nick, room);
        message.value = "";
    };

    socket.on("chatSendMessageToPlayers", function(message, nick) {
        var chat = document.getElementById("chatMessages");
        chat.scrollTop = chat.scrollHeight;
        chat.innerHTML += "<b>" + nick + ":</b> " + message + "<br/>";
    });

    socket.on("playerWinFromServer", function(nick, score, round) {

        document.getElementById("nick" + nick).innerHTML = nick + " - " + score;
        document.getElementById("round").innerHTML = "Liczba rund: <b> " + round + "/10 </b>";

    });

    createjs.Ticker.on("tick", tick);
    createjs.Ticker.setFPS(30);

    rightKey = leftKey = upKey = downKey = shootHeld = false;

    var lol = document.createElement("input");
    lol.type = "button";
    lol.value = "start";
    lol.id = "startGameButton";

    document.getElementById('lol').appendChild(lol);

    lol.onclick = function() {
        socket.emit('startGame', player.getRoom());
        lol.style.display = "none";
    };

}

function tick(event) {

    if (click) {
        stage.update(event);

        player.runController();

        boardAnimator.animateBullets();

        if (player.live) {
            collisionDetector.checkCars(player, opponents);
            collisionDetector.collisionMyCarAllBullet(player, arrayBullet);
            collisionDetector.checkBonus(player, bonusList);
        }
    }
}

