/**
 * Created by marek on 16.04.14.
 */

awesomegame.namespace("controller"); 

( function() {

		function CollisionDetector() {
		}


		awesomegame.controller.CollisionDetector = CollisionDetector;

		function handleComplete() {
			createjs.Ease.elasticOut;
		}


		CollisionDetector.prototype.checkCars = function(player, opponents) {
			var playerComponent = player.getCarComponent();
			for ( i = 0, n = opponents.length; i < n; i++) {

				col = ndgmr.checkPixelCollision(playerComponent, opponents[i].getCarComponent());

				if (opponents[i].live) {
					if (col) {
						if (col.x <= playerComponent.x && (playerComponent.x + 60 > 25) && (playerComponent.x + 60 < 775)) {

							var tween = createjs.Tween.get(playerComponent, {
								loop : false
							}).to({
								x : playerComponent.x + 60,
								rotation : 0
							}, 1000, createjs.Ease.getElasticOut(1, 0.8)).addEventListener("change", function() {
								socket.emit("PositionsFromPlayer", '{ "id": "' + player.getNick() + '", "room": "' + player.getRoom() + '", "x": "' + playerComponent.x + 60 + '", "y": "' + playerComponent.y + '", "rotation": "' + playerComponent.rotation + '" }');
							});
							//Ease.getElasticOut(1,0.8));

							playerComponent.rotation = 0;
							//socket.emit("PositionsFromPlayer", '{ "id": "' + player.getNick() + '", "room": "' + player.getRoom() + '", "x": "' + playerComponent.x+60 + '", "y": "' + playerComponent.y + '", "rotation": "' + playerComponent.rotation + '" }');

						} else if (col.x > playerComponent.x && (playerComponent.x - 60 > 25) && (playerComponent.x - 60 < 775)) {

							var tween = createjs.Tween.get(playerComponent, {
								loop : false
							}).to({
								x : playerComponent.x - 60,
								rotation : 0
							}, 1000, createjs.Ease.getElasticOut(1, 0.8));
							playerComponent.rotation = 0;

						} else if ((playerComponent.y - 60 > 25) && (playerComponent.y - 60 < 375)) {
							var tween = createjs.Tween.get(playerComponent, {
								loop : false
							}).to({
								y : playerComponent.y - 60,
								rotation : 0
							}, 1000, createjs.Ease.elasticOut);
							playerComponent.rotation = 0;
						} else {
							var tween = createjs.Tween.get(playerComponent, {
								loop : false
							}).to({
								y : playerComponent.y + 60,
								rotation : 0
							}, 1000, createjs.Ease.elasticOut);
							playerComponent.rotation = 0;
						}
					}
				}
			}
		};

		CollisionDetector.prototype.collisionMyCarAllBullet = function(player, arrayBullet) {

			for (i in arrayBullet) {
				var collisionCarBullet;
				if (arrayBullet[i][0] != undefined) {
					collisionCarBullet = ndgmr.checkPixelCollision(player.getCarComponent(), arrayBullet[i][0], 0);
				}
				if (collisionCarBullet && arrayBullet[i][1] != player.getCarComponent()) {

					socket.emit("playerHit", '{"id":"' + player.getNick() + '", "room":"' + player.getRoom() + '", "bulletIndex":"' + i + '"}');

					player.damage();
					stage.removeChild(arrayBullet[i][0]);
					arrayBullet.splice(i, 1);
				}
			}

		};

		CollisionDetector.prototype.checkBonus = function(player, bonusList) {
			activeBonuses = bonusList.getActiveBonuses();

			activeBonuses.forEach(function(bonus) {
				pickedUp = ndgmr.checkPixelCollision(player.getCarComponent(), bonus.getComponent());

				if (pickedUp) {
                    var help = {
                        id: player.getNick(),
                        room: player.getRoom(),
                        bonus: bonusList.getIndexOf(bonus)
                    }

                    socket.emit("playerPickedBonus", JSON.stringify(help));

				}
			});
		};

	}());
