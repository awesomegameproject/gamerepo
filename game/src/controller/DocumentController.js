/**
 * Created by marek on 10.04.14.
 */

awesomegame.namespace("controller");

(function () {

    var upKey, downKey, leftKey, rightKey, shootKey, timeTheLastShoot = 0;

    function DocumentController(carController) {

        this._carController = carController;
        document.onkeydown = handleKeyDown;
        document.onkeyup = handleKeyUp;
    }

    awesomegame.controller.DocumentController = DocumentController;	
	
    DocumentController.prototype.run = function () {

        if (shootKey) {
            if ((new Date()).getTime() > timeTheLastShoot + awesomegame.constants.intervalBulletTime) {
                timeTheLastShoot = (new Date()).getTime();
                this._carController.fire();
            }
        }

        if (upKey && downKey || rightKey && leftKey) {
            //inability to move both forward and backward and sideways at the same time
        } else if (upKey && leftKey)
            this.moveCarUpLeft(this._carController);
            
        else if (upKey && rightKey)
            this.moveCarUpRight(this._carController);
            
        else if (downKey && rightKey)
            this.moveCarDownRight(this._carController);
            
        else if (downKey && leftKey)
			this.moveCarDownLeft(this._carController);
			
        else if (upKey)			
            this.moveCarUp(this._carController);
            
        else if (downKey)	
            this.moveCarDown(this._carController);
            
        else
            this._carController.stop();
        
       this.updateOpponentsPositions();
    }
    
    DocumentController.prototype.moveCarUpLeft = function (carController) {
			if ((carController.car.x - Math.sin(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) >= 25 && (carController.car.x - Math.sin(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) <= 775 && (carController.car.y - Math.cos(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) >= 25 && (carController.car.y - Math.cos(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) <= 375)
				carController.moveUpLeft();
    }
    
    DocumentController.prototype.moveCarUpRight = function (carController) {
			if ((carController.car.x - Math.sin(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) >= 25 && (carController.car.x - Math.sin(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) <= 775 && (carController.car.y - Math.cos(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) >= 25 && (carController.car.y - Math.cos(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) <= 375)
				carController.moveUpRight();
    }
    
    DocumentController.prototype.moveCarDownLeft = function (carController) {
    			if ((carController.car.x + Math.sin(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) >= 25 && (carController.car.x + Math.sin(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) <= 775 && (carController.car.y + Math.cos(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) >= 25 && (carController.car.y + Math.cos(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) <= 375)
				carController.moveDownLeft();
    }
    
    DocumentController.prototype.moveCarDownRight = function (carController) {
			if ((carController.car.x + Math.sin(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) >= 25 && (carController.car.x + Math.sin(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) <= 775 && (carController.car.y + Math.cos(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) >= 25 && (carController.car.y + Math.cos(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) <= 375)
				carController.moveDownRight();  
    }
    
    DocumentController.prototype.moveCarUp = function (carController) {
			if ((carController.car.x - Math.sin(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) >= 25 && (carController.car.x - Math.sin(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) <= 775 && (carController.car.y - Math.cos(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) >= 25 && (carController.car.y - Math.cos(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) <= 375)
				carController.moveUp();
			else if (carController.car.rotation != 0) {
				if (carController.car.rotation % 360 > 0 && carController.car.rotation % 360 < 90 && (carController.car.x - Math.sin(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) <= 775)
					carController.car.y++;
				else if (carController.car.rotation % 360 > 90 && carController.car.rotation % 360 < 180 && (carController.car.y - Math.cos(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) <= 375)
					carController.car.x--;
				else if (carController.car.rotation % 360 > 180 && carController.car.rotation % 360 < 270 && (carController.car.x - Math.sin(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) >= 25)
					carController.car.y--;					
				else if (carController.car.rotation % 360 > 270 && carController.car.rotation % 360 < 360 && (carController.car.y - Math.cos(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) >= 25)
					carController.car.x++;
				//ujemne wartosci
				if (carController.car.rotation % 360 < 0 && carController.car.rotation % 360 > -90 && (carController.car.y - Math.cos(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) >= 25)
					carController.car.x++;
				else if (carController.car.rotation % 360 < -90 && carController.car.rotation % 360 > -180 && (carController.car.x - Math.sin(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) >= 25)
					carController.car.y--;
				else if (carController.car.rotation % 360 < -180 && carController.car.rotation % 360 > -270 && (carController.car.y - Math.cos(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) <= 375)
					carController.car.x--;
				if (carController.car.rotation % 360 < -270 && carController.car.rotation % 360 > -360 && (carController.car.x - Math.sin(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) <= 775)
					carController.car.y++;
				console.log((carController.car.rotation));
        }
    }
    
    DocumentController.prototype.moveCarDown = function (carController) {
			if ((carController.car.x + Math.sin(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) >= 25 && (carController.car.x + Math.sin(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) <= 775 && (carController.car.y + Math.cos(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) >= 25 && (carController.car.y + Math.cos(carController.car.rotation * (Math.PI / -180)) * carController.carSpeed) <= 375)
			carController.moveDown();
    }
    
   DocumentController.prototype.updateOpponentsPositions = function () {
        for (i = 0; i < opponents.length; ++i) {     
            if (opponents[i].lastMove != "stop") {  
                var ctrl = opponents[i].getCarController();
                 
                if (opponents[i].lastMove == "upLeft")
                    this.moveCarUpLeft(ctrl);
                    
                else if (opponents[i].lastMove == "upRight")
                    this.moveCarUpRight(ctrl);

                else if (opponents[i].lastMove == "downRight")
                    this.moveCarDownRight(ctrl);

                else if (opponents[i].lastMove == "downLeft")
                    this.moveCarDownLeft(ctrl);

                else if (opponents[i].lastMove == "up")
                    this.moveCarUp(ctrl);
                    
                else if (opponents[i].lastMove == "down")
                    this.moveCarDown(ctrl);
                
                ctrl.moveTo(ctrl.car.x, ctrl.car.y, ctrl.car.rotation);
            }
        }
    };

    function handleKeyDown(e) {

        if (!e) {
            var e = window.event;
        }

        switch (e.keyCode) {
            case KEYCODE_SPACE:
                shootKey = true;
                break;
            case KEYCODE_LEFT:
                leftKey = true;
                break;
            case KEYCODE_RIGHT:
                rightKey = true;
                break;
            case KEYCODE_UP:
                upKey = true;
                break;
            case KEYCODE_DOWN:
                downKey = true;
                break;
        }

    }

    function handleKeyUp(e) {
        if (!e) {
            var e = window.event;
        }
        switch (e.keyCode) {
            case KEYCODE_LEFT:
                leftKey = false;
                break;
            case KEYCODE_RIGHT:
                rightKey = false;
                break;
            case KEYCODE_UP:
                upKey = false;
                break;
            case KEYCODE_DOWN:
                downKey = false;
                break;
            case KEYCODE_SPACE:
                shootKey = false;
                break;
        }
    }

}());
