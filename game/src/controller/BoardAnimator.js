/**
 * Created by marek on 26.04.14.
 */

awesomegame.namespace("controller"); 

( function() {

    function BoardAnimator() {

    }

    awesomegame.controller.BoardAnimator = BoardAnimator;

    BoardAnimator.prototype.animateBullets = function() {
        for (bulletObject in arrayBullet) {

            arrayBullet[bulletObject][0].x -= awesomegame.constants.bulletSpeed * Math.sin(arrayBullet[bulletObject][0].rotation * (Math.PI / -180));
            arrayBullet[bulletObject][0].y -= awesomegame.constants.bulletSpeed * Math.cos(arrayBullet[bulletObject][0].rotation * (Math.PI / -180));

            if (arrayBullet[bulletObject][0].x >= 800 || arrayBullet[bulletObject][0].x <= 0 || arrayBullet[bulletObject][0].y >= 400 || arrayBullet[bulletObject][0].y <= 0) {
                stage.removeChild(arrayBullet[bulletObject][0]);
                arrayBullet.splice(bulletObject, 1);
            }
        }
    };

    BoardAnimator.prototype.activateBonus = function(i, x, y) {

        var currentIndex = bonusList.addBonus(i);
        bonusList.get(currentIndex).activate(x,y);

        setTimeout(function() {
            bonusList.get(currentIndex).deactivate();
        }, awesomegame.constants.bonusDisplayTime);

    };
}());
