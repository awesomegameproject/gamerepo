/**
 * Created by marek on 29.03.14.
 */
awesomegame.namespace("controller");

( function() {

		function CarController(carComponent, infoBar) {
			this.car = carComponent;
			this.playerInfoBar = infoBar;
			this.carSpeed = awesomegame.constants.carSpeed;
	        this.lastMove = "stop";
		}


		awesomegame.controller.CarController = CarController;

		CarController.prototype.moveTo = function(x, y, rotation) {
			this.car.x = x;
			this.car.y = y;
			this.car.rotation = rotation;

			this._increasePlayerInfoBarPositions();
		};

		CarController.prototype.feedPlayerData = function(nk, rm) {
			this.nick = nk;
			this.room = rm;
		};

		CarController.prototype.moveUpLeft = function() {
			this.car.rotation -= awesomegame.constants.carRotationDelta;

            this._changeCarValue("-");           
            if(this.lastMove != "upLeft")
            {
                this.lastMove = "upLeft";
                this.emit();
            }
		};

		CarController.prototype.moveUpRight = function() {
			this.car.rotation += awesomegame.constants.carRotationDelta;

            this._changeCarValue("-");
            if(this.lastMove != "upRight")
            {
                this.lastMove = "upRight";
                this.emit();
            }
		};

		CarController.prototype.moveDownRight = function() {
			this.car.rotation -= awesomegame.constants.carRotationDelta;

            this._changeCarValue("+");
            if(this.lastMove != "downRight")
            {
                this.lastMove = "downRight";
                this.emit();
            }
		};

		CarController.prototype.moveDownLeft = function() {
            this.car.rotation += awesomegame.constants.carRotationDelta;
            
            this._changeCarValue("+");
            if(this.lastMove != "downLeft")
            {
                this.lastMove = "downLeft";
                this.emit();
            }
		};

		CarController.prototype.moveUp = function() {

            this._changeCarValue("-");
			if(this.lastMove != "up")
            {
                this.lastMove = "up";
                this.emit();
            }
		};

		CarController.prototype.moveDown = function() {

            this._changeCarValue("+");
			if(this.lastMove != "down")
            {
                this.lastMove = "down";
                this.emit();
            }
		};

        CarController.prototype.stop = function() {

			if(this.lastMove != "stop")
            {
                this.lastMove = "stop";
                this.emit();
            }
		};
    


    CarController.prototype.damageCar = function () {
        var name = this.playerInfoBar.getChildByName("nickName"),
            newBitmap = new createjs.Bitmap("img/shoot1.png");
        newBitmap.alpha = 0.5;
        newBitmap.y = 20;
        newBitmap.name = "healthBar";
        this.playerInfoBar.removeAllChildren();
        this.playerInfoBar.addChild(name, newBitmap);
    }

    CarController.prototype._increasePlayerInfoBarPositions = function () {
        this.playerInfoBar.x = this.car.x - awesomegame.constants.playerInfoBarHeight;
        this.playerInfoBar.y = this.car.y - awesomegame.constants.playerInfoBarWidth;
    }


    CarController.prototype.getBullet = function () {
        return this.bullet;
    }

    CarController.prototype.fire = function () {
        var bullet = new awesomegame.view.Bullet();

        createjs.Sound.play("fireSound");
        
        bulletComponent = bullet.getBulletComponent();
        bulletComponent.x = this.car.x;
        bulletComponent.y = this.car.y;
        bulletComponent.rotation = this.car.rotation;
        stage.addChild(bulletComponent);
        arrayBullet[arrayBullet.length] = [bulletComponent, this.car]; //zmiana


		socket.emit("bulletFromPlayer", '{ "id": "' + this.nick + '", "room": "' + this.room + '", "x": "' + bulletComponent.x + '", "y": "' + bulletComponent.y + '", "rotation": "' + bulletComponent.rotation + '", "lastMove": "undefined" }');
    }
    
	CarController.prototype.changeSpeed = function(operation, deltaSpeed) {
	    switch (operation) {
		    case "+":
			    this.carSpeed += deltaSpeed;
			    break;
		    case "-":
			    this.carSpeed -= deltaSpeed;
			    break;
	    }
	    this.emit();
    };

    CarController.prototype._changeCarValue = function (operation) {
        switch (operation) {
            case "+":
                this.car.x += Math.sin(this.car.rotation * (Math.PI / -180)) * this.carSpeed;
                this.car.y += Math.cos(this.car.rotation * (Math.PI / -180)) * this.carSpeed;
                break;
            case "-":
                this.car.x -= Math.sin(this.car.rotation * (Math.PI / -180)) * this.carSpeed;
                this.car.y -= Math.cos(this.car.rotation * (Math.PI / -180)) * this.carSpeed;
                break;
        }
        this._increasePlayerInfoBarPositions();
        this.emit();
    }
    
    CarController.prototype.emit = function () {
        socket.emit("DataFromPlayer", '{ "id": "' + this.nick + '", "room": "' + this.room + '", "speed": "' + this.carSpeed + '", "lastMove": "' + this.lastMove + '" }');
    }
    
}());
