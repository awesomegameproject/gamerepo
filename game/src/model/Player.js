awesomegame.namespace("model");

( function() {

		function Player(nick, room) {
			this._nickName = nick;
			this._roomName = room;
			this._infoBar = new awesomegame.helpers.InfoBarBuilder(this._nickName);
			this._car = new awesomegame.view.Car(this._infoBar, this._position);

			this.documentController = new awesomegame.controller.DocumentController(this._car.getCarController());
			this._car.getCarController().feedPlayerData(this._nickName, this._roomName);

			this.backToLive();
		}


		awesomegame.model.Player = Player;

		Player.prototype.damage = function() {
			this._health -= 1;

			switch (this._health) {
				case 2:
					this._infoBar = new awesomegame.helpers.HealthBarReplacer("img/shoot1.png", this._infoBar);
					break;
				case 1:
					this._infoBar = new awesomegame.helpers.HealthBarReplacer("img/shoot2.png", this._infoBar);
					break;
				case 0:
					this.kill();
					return;
			}
		};

		Player.prototype.getCar = function() {
			return this._car;
		};

		Player.prototype.getCarComponent = function() {
			return this._car.getCarComponent();
		};

		Player.prototype.runController = function(opponents) {
			if (this.live) {
				this.documentController.run(opponents);
			}
		};

		Player.prototype.getInfoBar = function() {
			return this._infoBar;
		};

		Player.prototype.getNick = function() {
			return this._nickName;
		};

		Player.prototype.getRoom = function() {
			return this._roomName;
		};

		Player.prototype.setPositions = function(x, y) {
			this._position = new awesomegame.helpers.Position(x, y);
			this._car.setCarPositions(this._position);
			this._infoBar.x = x - awesomegame.constants.playerInfoBarHeight;
			this._infoBar.y = y - awesomegame.constants.playerInfoBarWidth;
		};

		Player.prototype.backToLive = function() {
			this._health = 3;
			this.live = true;
			this._infoBar = new awesomegame.helpers.HealthBarReplacer("img/full.png", this._infoBar);

			stage.addChild(this.getCarComponent());
			stage.addChild(this.getInfoBar());
		};

		Player.prototype.kill = function() {
			this.live = false;

			stage.removeChild(this._infoBar);
			stage.removeChild(this.getCarComponent());
		};


}()); 
