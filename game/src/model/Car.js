awesomegame.namespace("view");

( function () {

    function Car(infoBar) {
        this._carComponent = new createjs.Bitmap("img/a2.png");

        this._carComponent.regX = awesomegame.constants.carRegX;
        this._carComponent.regY = awesomegame.constants.carRegY;

        this._carController = new awesomegame.controller.CarController(this._carComponent, infoBar);
    }

    awesomegame.view.Car = Car;

    Car.prototype.setCarPositions = function (pos) {
        this.position = pos;
        this._carComponent.x = pos.getX();
        this._carComponent.y = pos.getY();
    };

    Car.prototype.getCarComponent = function () {
        return this._carComponent;
    };
    Car.prototype.getCarController = function () {
        return this._carController;

    };

}());