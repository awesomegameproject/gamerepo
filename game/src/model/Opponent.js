/**
 * Created by marek on 08.04.14.
 */

awesomegame.namespace("model");

//Opponent
( function() {

		function Opponent(nick) {
			this._nickName = nick;
			this._infoBar = new awesomegame.helpers.InfoBarBuilder(this._nickName);
			this._car = new awesomegame.view.Car(this._infoBar);
			this.backToLive();
		}


		awesomegame.model.Opponent = Opponent;

		Opponent.prototype.damage = function() {
			this._health -= 1;

			switch (this._health) {
				case 2:
					this._infoBar = new awesomegame.helpers.HealthBarReplacer("img/shoot1.png", this._infoBar);
					break;
				case 1:
					this._infoBar = new awesomegame.helpers.HealthBarReplacer("img/shoot2.png", this._infoBar);
					break;
				case 0:
					this.kill();
					return;
			}
		};

		Opponent.prototype.getCarComponent = function() {
			return this._car.getCarComponent();
		};

		Opponent.prototype.setPositions = function(x, y) {
			this._position = new awesomegame.helpers.Position(x, y);
			this._car.setCarPositions(this._position);
			this._infoBar.x = x - awesomegame.constants.playerInfoBarHeight;
			this._infoBar.y = y - awesomegame.constants.playerInfoBarWidth;
		};
		
		Opponent.prototype.setLastMove = function(lastMove) {
			this.lastMove = lastMove;
		};
		
		Opponent.prototype.setSpeed = function(speed) {
			var carController = this.getCarController();
			carController.carSpeed = speed;
		};

		Opponent.prototype.getCarController = function() {
			return this._car.getCarController();
		};

		Opponent.prototype.getInfoBar = function() {
			return this._infoBar;
		};

		Opponent.prototype.getNick = function() {
			return this._nickName;
		};

		Opponent.prototype.backToLive = function() {
			this.live = true;
			this._health = 3;
			this._infoBar = new awesomegame.helpers.HealthBarReplacer("img/full.png", this._infoBar);
            this.lastMove = "stop";

			stage.addChild(this.getCarComponent());
			stage.addChild(this.getInfoBar());
		};

		Opponent.prototype.kill = function() {
			this.live = false;

			stage.removeChild(this._infoBar);
			stage.removeChild(this.getCarComponent());
		};

	}());
