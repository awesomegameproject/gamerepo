/**
 * Created by marek on 08.04.14.
 */

awesomegame.namespace("helpers"); 

( function() {

		function InfoBarBuilder(nickName) {
			var __nickText, __healthBar;

			this._infoBar = new createjs.Container();

			__nickText = new createjs.Text(nickName, "bold 12px Arial", "#000000");
			__nickText.name = "nickName";

			__healthBar = new createjs.Bitmap("img/full.png");
			__healthBar.name = "healthBar";
			__healthBar.alpha = 0.5;
			__healthBar.y = 20;

			this._infoBar.addChild(__nickText, __healthBar);

			return this._infoBar;
		}


		awesomegame.helpers.InfoBarBuilder = InfoBarBuilder;

	}()); 