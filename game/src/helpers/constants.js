awesomegame.namespace("constants");

( function () {

    awesomegame.constants.carRegX = 11;
    awesomegame.constants.carRegY = 25;
    awesomegame.constants.carX = 200;
    awesomegame.constants.carY = 200;
    awesomegame.constants.carRotationDelta = 5;
    awesomegame.constants.carSpeed = 5;
    
    awesomegame.constants.playerInfoBarHeight = 20;
    awesomegame.constants.playerInfoBarWidth = 70;
    
    awesomegame.constants.bulletScaleX = 0.6;
    awesomegame.constants.bulletScaleY = 0.6;
    awesomegame.constants.bulletRegX = 7;
    awesomegame.constants.bulletRegY = 14;
    awesomegame.constants.bulletSpeed = 20;
    awesomegame.constants.intervalBulletTime = 1000;
    
    awesomegame.constants.intervalBonusTime = 1000;
    awesomegame.constants.bonusDisplayTime = 5000;
    
    awesomegame.constants.bonusDuration = {
    	speedUp: 1000
    };
    
    awesomegame.constants.syncInterval = 1500;

}());
