/**
 * Created by marek on 26.04.14.
 */

awesomegame.namespace("helpers"); 

( function() {

		function HealthBarReplacer(path, infoBar) {

			this._infoBar = infoBar;
			var name = this._infoBar.getChildByName("nickName"), newBitmap;

			newBitmap = new createjs.Bitmap(path);

			newBitmap.alpha = 0.5;
			newBitmap.y = 20;
			newBitmap.name = "healthBar";
			this._infoBar.removeAllChildren();
			this._infoBar.addChild(name, newBitmap);

			return this._infoBar;
		}


		awesomegame.helpers.HealthBarReplacer = HealthBarReplacer;

	}());
