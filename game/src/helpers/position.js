/**
 * Created by marek on 08.04.14.
 */

awesomegame.namespace("helpers"); 

( function() {

		function Position(x, y) {
			this.x = x;
			this.y = y;
		}


		awesomegame.helpers.Position = Position;

		Position.prototype.getX = function() {
			return this.x;
		};

		Position.prototype.getY = function() {
			return this.y;
		};
	}()); 