var awesomegame = (function () {
    function namespace(string) {
        var object = this;
        var levels = string.split(".");
        for (var i = 0, l = levels.length; i < l; i++) {
            if (typeof object[levels[i]] == "undefined") {
                object[levels[i]] = {};
            }
            object = object[levels[i]];
        }
        return object;
    }

    return {
        namespace: namespace
    };
}());

awesomegame.isOwnProperty = (function () {
    var hasOwn = Object.prototype.hasOwnProperty;
    if (typeof hasOwn == "function") {
        return function (object, property) {
            return hasOwn.call(object, property);
        };
    } else {
        // Provide an emulation if you can live with possibly
        // inaccurate results
    }
}());
