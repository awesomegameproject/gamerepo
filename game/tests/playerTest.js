TestCase("PlayerCreationTest", {
    "test should give correct Object": function () {
        var player = new awesomegame.model.Player();

        assertObject(player);
    }
});

TestCase("PlayerInfoBarTest", {
    "test should give correct position InfoBar": function () {
        var player = new awesomegame.model.Player("TestMaster"),
            infoBar = player.getInfoBar();

        assertObject(infoBar);

        assertEquals(awesomegame.constants.carX - awesomegame.constants.playerInfoBarHeight, infoBar.x);
        assertEquals(awesomegame.constants.carY - awesomegame.constants.playerInfoBarWidth, infoBar.y);

        player.getCar().getCarController().damageCar();

        assertEquals("TestMaster", infoBar.getChildByName("nickName").text);
        assertEquals("\<img src=\"img/shoot1.png\"\>", infoBar.getChildByName("healthBar").image.outerHTML);

    }
});