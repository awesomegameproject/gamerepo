/**
 * Created by marek on 16.04.14.
 */

TestCase("CollisionBeetwenCarsTest", {
    "test check if two cars are in collision": function () {
        var player = new awesomegame.model.Player("Test", "ONE"),
            position = new awesomegame.helpers.Position(10, 10),
            opponent = new awesomegame.model.Opponent("OPTest", position),
            circle3;


        player.getCarComponent().x = 100;
        player.getCarComponent().y = 100;

        opponent.getCarComponent().x = 100;
        opponent.getCarComponent().y = 100;

        circle3 = new createjs.Bitmap("img/h.png");
//    circle3.graphics.beginFill("red").drawRect(0, 0, 40, 40);
        circle3.regX = circle3.regY = 20;
        circle3.x = circle3.y = 100;
        //Add Shape instance to stage display list.

        var i = 0, collision;
        while (i < 20) {
            opponent.getCarComponent().x -= 1;
            collision = ndgmr.checkPixelCollision(player.getCarComponent(), circle3, 0.75);
            if (collision) {
                console.log("kolizja!!");
            }
            i++;
        }


    }
});