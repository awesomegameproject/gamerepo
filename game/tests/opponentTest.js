/**
 * Created by marek on 10.04.14.
 */

TestCase("OpponentsCreationTest", {
    "test should correctly create a few opponents": function () {

        var opponentPosition1 = new awesomegame.helpers.Position(300, 300),
            opponentPosition2 = new awesomegame.helpers.Position(100, 50),
            opponent1 = new awesomegame.model.Opponent("Łukasz", opponentPosition1),
            opponent2 = new awesomegame.model.Opponent("Pestek", opponentPosition2),
            player = new awesomegame.model.Player("Master", new awesomegame.helpers.Position(10, 10));

        assertEquals("Łukasz", opponent1.getNick());
        assertEquals("Pestek", opponent2.getNick());
        assertEquals("Master", player.getNick());

        assertNotEquals(opponent1.getCarComponent(), opponent2.getCarComponent());

    }
});

TestCase("OpponentMovingTest", {
    "test should correctly move opponent car": function () {
        var opponentPosition1 = new awesomegame.helpers.Position(300, 300),
            opponent1 = new awesomegame.model.Opponent("Łukasz", opponentPosition1);

        opponent1.getCarController().moveDown();

        assertEquals(305, opponent1.getCarComponent().y);

    }
});
