/**
 * Created by marek on 05.04.14.
 */

TestCase("CarReceivingTest", {
    "test should give correct Car positions from Player": function () {
        var player = new awesomegame.model.Player(),
            car, carPositions;

        car = player.getCar();

        carPositions = car.getCarPositions();

        assertObject(car);
        assertEquals(carPositions.x, awesomegame.constants.carX);
        assertEquals(carPositions.y, awesomegame.constants.carY);
        assertEquals(carPositions.regX, awesomegame.constants.carRegX);
        assertEquals(carPositions.regY, awesomegame.constants.carRegY);
    }
});

TestCase("CarMovingTest", {
    "test should correctly turn our Car": function () {
        var player = new awesomegame.model.Player(),
            car, carController;

        car = player.getCar();
        carController = car.getCarController();

        assertObject(carController);

        carController.moveUp();
        assertEquals(awesomegame.constants.carY - 5, car.getCarComponent().y);

        carController.moveDown();
        assertEquals(awesomegame.constants.carY, car.getCarComponent().y);

    }
});