
var server = server || {}; // the only global variable
server.MODEL = server.MODEL || {}; // model of our game
server.CONTROLLER = server.CONTROLLER || {}; //controller number one  
server.APPLICATION = server.APPLICATION || {};  //controller number two

//Player
(function () {

    function Player(id, room, socket) {
        this._id = id;
        this._room = room;
        this._socket = socket;
        this._score = 0;
    }

    Player.prototype.getId = function () {
        return this._id;
    }

    Player.prototype.newScore = function () {
        this._score += 1;
    }

    Player.prototype.getScore = function () {
        return this._score;
    }

    Player.prototype.getRoom = function () {
        return this._room;
    }

    Player.prototype.getBar = function () {
        return this._bar;
    }

    Player.prototype.getSocket = function () {
        return this._socket;
    }

    server.MODEL.Player = Player;

}());

//Room
(function () {

    function Room() {
        this._players = [];
        this._busy = false;
        this._gameCounter = 1;
        this._numberOfGame = 2; //current max three games
    }

    Room.prototype.addPlayer = function (player) {
        this._players[player.getId()] = player;
    }

    Room.prototype.isBusy = function () {
        return this._busy;
    }

    Room.prototype.newGame = function () {
        this._gameCounter += 1;
    }

    Room.prototype.blockRoom = function () {
        this._busy = true;
    }

    Room.prototype.unBlockRoom = function () {
        this._busy = false;
    }

    Room.prototype.removePlayer = function (player) {
        delete this._players[player.getId()];
    }

    Room.prototype.getPlayers = function () {
        return this._players;
    }

    Room.prototype.getGameCounter = function () {
        return this._gameCounter;
    }

    Room.prototype.getPlayerOfId = function (playerId) {
        return this._players[playerId];
    }

    Room.prototype.isEnd = function () {
        return (this._gameCounter - 1 == this._numberOfGame);
    }

    server.MODEL.Room = Room;

}());

//RoomList
(function () {

    function Rooms() {
        this._rooms = [];
    }

    Rooms.prototype.addRoom = function (room, id) {
        this._rooms[id] = room;
    }

    Rooms.prototype.removeRoom = function (id) {
        delete this._rooms[id];
    }

    Rooms.prototype.getRooms = function () {
        return this._rooms;
    }

    Rooms.prototype.getRoomOfId = function (roomId) {
        return this._rooms[roomId];
    }

    server.MODEL.Rooms = new Rooms();

}());

//PlayerList
(function () {

    function PlayersList() {
        this._listOfPlayers = [];
    }

    PlayersList.prototype.addPlayer = function (socket, player) {
        this._listOfPlayers[socket.id] = player;
    }

    PlayersList.prototype.removePlayer = function (socket) {
        delete this._listOfPlayers[socket];
    }

    PlayersList.prototype.getPlayer = function (socket) {
        return this._listOfPlayers[socket];
    }

    PlayersList.prototype.getList = function () {
        return this._listOfPlayers;
    }

    server.MODEL.PlayersList = new PlayersList();

}());

//Arrow
(function () {

    function Arrow(x, y, rotation) {
        this._x = x;
        this._y = y;
        this._rotation = rotation;
    }

    Arrow.prototype.getX = function () {
        return this._x;
    }

    Arrow.prototype.getY = function () {
        return this._y;
    }

    Arrow.prototype.getRotation = function () {
        return this._rotation;
    }

    server.MODEL.Arrow = Arrow;

}());

//site

(function () {

    function answerDataFromSiteController() {
    }

    answerDataFromSiteController.prototype.newPlayer = function (data, socket) {
        var helper,
            newPlayer;

        helper = JSON.parse(data);
        newPlayer = new server.MODEL.Player(helper.id, helper.room, socket);
        server.MODEL.Rooms.getRooms()[helper.room] = server.MODEL.Rooms.getRooms()[helper.room] || new server.MODEL.Room();
        server.MODEL.Rooms.getRooms()[helper.room].addPlayer(newPlayer);
        server.MODEL.PlayersList.addPlayer(socket, newPlayer);
    }

    server.CONTROLLER.answerDataFromSiteController = server.CONTROLLER.answerDataFromSiteController || new answerDataFromSiteController();

}());

(function () {

    function BonusController(rr) {
        this.room = rr;
    }

    BonusController.prototype.randomBonus = function () {

        var len = 1,
            rand = Math.floor(Math.random() * len);

        return rand;
    }

    BonusController.prototype.randomPositions = function () {
        var xx, yy;

        xx = Math.floor((Math.random() * 700)) + 50;
        yy = Math.floor((Math.random() * 700)) + 50;

        return {x: xx, y: yy};
    }

    BonusController.prototype.run = function (players) {
        var bonusController = this,
            players;

        players = server.MODEL.Rooms.getRoomOfId(this.room).getPlayers();

        setInterval( function () {
            var index,
                iterator;

            index = bonusController.randomBonus();
            randPosition = bonusController.randomPositions();

            for (iterator in players) {
                players[iterator].getSocket().emit("receiveBonus", index, randPosition.x, randPosition.y);
            }

        }, 1000);

    }

    server.CONTROLLER.BonusController = BonusController;

}());

//server run
(function() {
    var express = require('express'),
        sio = require('socket.io'),
        http = require('http');


    server.APPLICATION.express = express();
    server.APPLICATION.express.use(express.bodyParser());
    server.APPLICATION.express.use(express.static('../../game'));
    //server.APPLICATION.express.use(express.static('../view'));

    server.APPLICATION.server = http.createServer(server.APPLICATION.express);
    server.APPLICATION.server.listen(3000);

    server.APPLICATION.socket = sio.listen(server.APPLICATION.server);


}());


(function () {

    server.APPLICATION.socket.on('connection', function (socket) {

        socket.emit('helloFromServer', 'Witamy w awesomegame!');

        socket.on('joinNewPlayer', function (data) {
            var helper,
                players,
                iterator;

            server.CONTROLLER.answerDataFromSiteController.newPlayer(data, socket);
            helper = JSON.parse(data);
            players = server.MODEL.Rooms.getRoomOfId(helper.room).getPlayers();

            for (iterator in players) {
                //don't send it to yourself
                if (players[iterator].getId() != helper.id) {
                    players[iterator].getSocket().emit("sendNewPlayerToAllPlayersInRoom", helper.id);
                    socket.emit("sendCurrentPlayersInRoomToNewPlayer", players[iterator].getId());
                }
            }
        });

        socket.on('DataFromPlayer', function (data) {
            var iterator,
                players,
                room,
                helper;

            helper = JSON.parse(data);
            room = helper.room;

           if (room != "undefined")
                players = server.MODEL.Rooms.getRoomOfId(room).getPlayers();
                
            for (iterator in players) {
                players[iterator].getSocket().emit("sendNewPositionsToAllPlayersInRoom", data);
            }
        })

        socket.on('bulletFromPlayer', function (bullet) {
            var iterator,
                players,
                room,
                helper;

            helper = JSON.parse(bullet);
            room = helper.room;
            players = server.MODEL.Rooms.getRoomOfId(room).getPlayers();

            for (iterator in players) {
                players[iterator].getSocket().emit("sendNewBulletToAllPlayersInRoom", bullet);
            }
        })

        socket.on('startGame', function (room) {
            var iterator,
                players,
                guard = true,
                previousPositions = [],
                x,
                y,
                previousPositionsIterator;

            var pomoc = [];
            players = server.MODEL.Rooms.getRoomOfId(room).getPlayers();

            for (iterator in players) {
                while (true) {
                    guard = true;
                    x = Math.floor((Math.random() * 700)) + 50; //return a random number between 0 and 800
                    y = Math.floor((Math.random() * 300)) + 50; // return a random number between 0 and 400
                    for (previousPositionsIterator in previousPositions) {

                        if (Math.abs(x - JSON.parse(previousPositions[previousPositionsIterator]).x) + Math.abs(y - JSON.parse(previousPositions[previousPositionsIterator]).y) < 50) {
                            guard = false;
                            break;
                        }
                    }

                    if (guard) {
                        previousPositions.push('{"id":"' + players[iterator].getId() + '", "x":"' + x + '", "y": "' + y + '"}');
                        break;
                    }
                }
            }

            var bonusController = new server.CONTROLLER.BonusController(room);
            bonusController.run();

            for (iterator in players) {
                players[iterator].getSocket().emit("startGameFromServer", previousPositions);
            }

        })

        socket.on('playerHit', function (data) {
            var iterator,
                players,
                room,
                helper;

            helper = JSON.parse(data);
            room = helper.room;
            players = server.MODEL.Rooms.getRoomOfId(room).getPlayers();

            for (iterator in players) {
                players[iterator].getSocket().emit("sendNewHitToAllPlayersInRoom", data);
            }

        });

        socket.on('playerPickedBonus', function (data) {

            var parser = JSON.parse(data),
                players = server.MODEL.Rooms.getRoomOfId(parser.room).getPlayers(),
                iterator;

             for (iterator in players) {
                players[iterator].getSocket().emit("playerPickedBonusFromServer", data);
            }

        });

        socket.on('playerWin', function (data) {
            var iterator,
                players,
                room,
                id,
                html,
                helper;


            helper = JSON.parse(data);
            room = helper.room;
            id = helper.id;


            server.MODEL.Rooms.getRoomOfId(room).newGame();
            server.MODEL.Rooms.getRoomOfId(room).getPlayerOfId(id).newScore();
            players = server.MODEL.Rooms.getRoomOfId(room).getPlayers();

            for (iterator in players) {
                players[iterator].getSocket().emit("playerWinFromServer", helper.id, server.MODEL.Rooms.getRoomOfId(room).getPlayerOfId(id).getScore(), server.MODEL.Rooms.getRoomOfId(room).getGameCounter());
            }


            if (server.MODEL.Rooms.getRoomOfId(room).isEnd()) {
                html = '<div id="ranking"> <ul>';
                for (iterator in players) {
                    html += '<li> ' + players[iterator].getId() + ' - ';
                    html += players[iterator].getScore() + '</li> <br/>';
                }
                html += '</ul></div>';
                for (iterator in players) {
                    players[iterator].getSocket().emit("theEnd", html);
                }
                server.MODEL.Rooms.removeRoom(room);
            }
        })
        
        socket.on('syncPositionsRequest', function(data) {
            var iterator,
                players,
                room,
                id,
                helper;

            helper = JSON.parse(data);
            room = helper.room;
            id = helper.id;

            players = server.MODEL.Rooms.getRoomOfId(room).getPlayers();
            console.log(id);
            for (iterator in players) {
                var player = players[iterator];
                
                if(player._id != id)
                {
                    console.log(player._id);
                    players[iterator].getSocket().emit("sync", data);
                }
            } 
        })
        
        socket.on('chatSendMessageToServer', function (message, nick, room) {
            var iterator,
                players;

            players = server.MODEL.Rooms.getRoomOfId(room).getPlayers();

            for (iterator in players) {
                if(players[iterator].getId() != nick) {
                    players[iterator].getSocket().emit("chatSendMessageToPlayers", message, nick);
                }
            }

        })





        /*
        socket.on('disconnect', function () {
            console.log(server.MODEL.PlayersList.getPlayer(socket.id).getId());
            console.log(server.MODEL.PlayersList.getPlayer(socket.id).getRoom());

            console.log("kto spierdolilllll");
            var pom = server.MODEL.PlayersList.getList();
            for (var i in pom) {
                console.log(pom[i]);
            }

        });*/

    });


}())
